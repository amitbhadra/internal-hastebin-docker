This is an ansible project which sets up a docker for an internal pastebin.
On your browser, you can open hastebin by localhost:7777 The files are stored at the location /apps/hastebin/data


I've added tags separately. 

For the very first time, in case Docker is not already installed, use:

$ansible-playbook main_playbook.yml --tags "install"

So when you start the docker, use:

$ansible-playbook main_playbook.yml --tags "start"

To stop use:

$ansible-playbook main_playbook.yml --tags "stop"